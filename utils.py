#!/usr/bin/env python3
import re
from lxml import etree
from .schemas import NMSPACE
from .templates import page_break, sectPr

def trash_remover(string):
    """
    Принимает элемент lxml (lxml._Element) проверяет, упаковывает и отдает обратно
    При проверке удаляюся
    """
    reg = r'\s((w|w[\d]+):((rsid[\w]+)|([\w]+Id))=\"[\w\d]+\")'
    string = etree.tostring(string).decode('utf8')
    string = re.sub(reg, '', string)
    string = etree.fromstring(string)
    return string

def fix_sectPr(sect):
    """
    Установка свойств размера страницы и отступов
    """
    pg_sz = sect.xpath('//w:pgSz', namespaces=NMSPACE) # размер сраницы
    for size in pg_sz:
        size.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}w',
              '11906')
        size.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}h',
              '16838')

    pg_mar = sect.xpath('//w:pgMar', namespaces=NMSPACE) # отступы
    for margin in pg_mar:
        margin.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}top',
              '1134')
        margin.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}right',
              '850')
        margin.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}bottom',
              '1134')
        margin.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}left',
              '1701')
        margin.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}header',
              '708')
        margin.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}footer',
              '708')
        margin.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}gutter',
              '0')

def set_rsids(elem):
    rsidR = rsidP = rsidRDefault = '00871D1E'
    rsidRPr = '00F43C80'
    elem.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}rsidR', rsidR)
    elem.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}rsidP', rsidP)
    elem.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}rsidRDefault', rsidRDefault)
    elem.set('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}rsidRPr', rsidRPr)
    return elem


    return elem

class MergeMixin:
    """
    Слияние файлов .docx
    """

    def __get_footnotes_id(self, elem):
        """
        Поиск сносок в элементе
        Возвращает список старых id сносок
        """
        el = elem.xpath('.//w:footnoteReference/@w:id', namespaces=NMSPACE)
        try:
            return [int(i) for i in el]
        except ValueError:
            pass
        return el

    def __get_footnotes_by_old_id(self, idx):
        for el in self.footnotes.body:
            if el['old_id'] == idx:
                return el['elem']

    def merge_docx(self, file_for_merge):
        add_file = self.__class__(file_for_merge)
        # перебор содеримого document.xml
        for elem in add_file.document.body:
            # поиск сносок
            replaces_id = {}
            if add_file.__get_footnotes_id(elem):
                for fn in add_file.__get_footnotes_id(elem):
                    # {old_id: elem} нужно для исправлени id ссылки в теле документа
                    replaces_id.update({str(fn): add_file.__get_footnotes_by_old_id(fn)})
                # добавление сносок в self.footnotes
                for fn in replaces_id:
                    self.footnotes += {'old_id': fn, 'elem': replaces_id[fn]}
                    replaces_id.update({fn: self.footnotes._get_last_id()}) # {old_id: new_id}
                # сноски в тексте
                elem_fn = elem.xpath('.//w:footnoteReference', namespaces=NMSPACE)
                for fn in elem_fn:
                    old_id = fn.xpath('@w:id', namespaces=NMSPACE)[0] # старый id
                    fn.set(fn.keys()[0], str(replaces_id[old_id])) # превращаяется в новый

            # добавление одержимого
            self.document += elem
