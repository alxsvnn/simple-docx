#!/usr/bin/env python3


class ErrPath(Exception):
    """Ошибка чтения файла или неверный путь"""

class ErrNeededFile(Exception):
    """Повреждена структура файла"""

class ErrDocumentIAdd(Exception):
    """Невозможно прибавить элемент к документу. Элемент не является потомком """