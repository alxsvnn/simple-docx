#!/usr/bin/env python3
from copy import copy
from lxml import etree
from .templates import sectPr
from .schemas import NMSPACE
from .validation import check_etree_element, check_sectpr
from .utils import trash_remover, set_rsids


class BaseElement:
    def __init__(self, struct=None):
        self._struct = struct
        self.__input_body = self.__get_input_body()
        self.body = self.__get_input_body()

    def __str__(self):
        return ', '.join([str(i) for i in self.body])

    def __repr__(self):
        return str(self.body)

    def __len__(self):
        return len(self.body)

    def __get_input_body(self):
        try:
            elems = self._struct.getchildren()
            return [i for i in elems]
        except IndexError:
            return []
        except AttributeError:
            return []


class BaseDocument(BaseElement):
    """
    Структура элементов document.xml и работа с ними
    """
    def __init__(self, struct=None):
        super(BaseDocument, self).__init__(struct)
        self.__input_body = self.__get_input_body()
        self.body = copy(self.__input_body)

    def __iadd__(self, other):
        """
        Элементы добавляеются, само собой, поштучно и только так.
        Элемент добавления в тело документа обязательно должет быть
        потомком etree._Element, иначе вызывается ошибка ErrDocumentIAdd
        """
        check_etree_element(other) # проверка элемента
        new_elem = set_rsids(other)
        if check_sectpr(new_elem): self.body.append(new_elem)
        return self

    def __get_input_body(self):
        try:
            elems = self._struct.xpath('//w:body', namespaces=NMSPACE)[0].getchildren()
            return [i for i in elems]
        except IndexError:
            return []
        except AttributeError:
            return []

    def _get_body(self):
        return self._struct.xpath('//w:body', namespaces=NMSPACE)[0].getchildren()

    def _rebuild_xml(self):
        """
        При вызове функции save() из основного класса Docx запускается
        пересборка структуры файла для последующего сохранения
        w:document>w:body>elements
        """
        rebuild_xml = self._struct
        body_xml = self._struct.getchildren()[0]
        [self._struct.remove(i) for i in rebuild_xml] # чистый w:document
        [body_xml.remove(i) for i in body_xml] # чистый w:body
        [body_xml.append(i) for i in self.body] # пересборка w:body
        body_xml.append(sectPr())
        self._struct.append(body_xml) # добавление w:body в w:document


class BaseFootnotes(BaseElement):
    """
    Структура элементов footnotes.xml и работа с ними
    """
    def __init__(self, struct=None):
        super(BaseFootnotes, self).__init__(struct)
        self.__input_body = self.__get_input_body()
        self.body = self.__get_body()

    def __str__(self):
        return ', '.join([str(i['elem']) for i in self.body])

    def __iadd__(self, other):
        num = len(self.body) + 1
        if isinstance(other, dict):
            other.update({'id': num}) # обновление id
            # чистка от мусора аля w:rsid\etc...
            tmp_obj = trash_remover(other['elem'])
            other['elem'] = tmp_obj; del tmp_obj
            # установка id
            other = self.__set_new_id(other)
            # установка id стилей сноски
            other['elem'] = self.__set_new_style_id(other['elem'])

            self.body.append(other)
        return self

    def __get_input_body(self):
        try:
            elems = self._struct.xpath('//w:footnote', namespaces=NMSPACE)
            return [i for i in elems if self.__is_footnote(i)]
        except IndexError:
            return []
        except AttributeError:
            return []

    def _get_body(self):
        return self._struct.xpath('//w:footnote', namespaces=NMSPACE)

    def __get_body(self):
        body = [{'id': index+1,
                 'elem': elem,
                 'old_id': self.__get_id(elem)} \
            for index, elem in enumerate(self.__input_body)]
        return body

    def __is_footnote(self, item):
        """
        Проверка заданного id.
        -1 и 0: технически, они не нужны
        1,2,...,N: сноски
        """
        if self.__get_id(item) >= 1:
            return True

    def __get_id(self, item):
        """
        Получение id сноски
        """
        return int(item.xpath('@w:id', namespaces=NMSPACE)[0])

    def _get_last_id(self):
        if len(self.body):
            return self.body[-1]['id']

    def __set_new_id(self, item):
        """
        Усановка id сноски
        """
        new_item = item['elem']
        new_item.set(
            '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}id',
            str(item['id']))
        item['obj'] = new_item
        return item

    def __set_new_style_id(self, item):
        """
        Установка id стилей сноски
        """
        val = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val'
        p_st = 'a3'; r_st = 'a5'

        ps = item.xpath('//w:pStyle', namespaces=NMSPACE)
        [st.set(val, p_st) for st in ps]

        rs = item.xpath('//w:rStyle', namespaces=NMSPACE)
        [st.set(val, r_st) for st in rs]

        return item

    def _rebuild_xml(self):
        """
        При вызове функции save() из основного класса Docx запускается
        пересборка структуры файла для последующего сохранения
        w:footnotes>elements
        """
        rebuild_xml = self._struct
        if not len(rebuild_xml): # выход из функции, если нет сносок
            return None
        [rebuild_xml.remove(i) for i in self._struct if self.__get_id(i) > 0]
        [rebuild_xml.append(i['elem']) for i in self.body]
        self._struct = rebuild_xml