#!/usr/bin/env python3
from lxml import etree
from .error import ErrNeededFile, ErrDocumentIAdd
from .schemas import NMSPACE

def check_needed_files_in_archive(files):
    """
    Вызывается в служебной функции __file_list класса Base
    для проверки наличия нужных для парсинг файлов
    """
    status = True
    needed = ('document.xml', 'styles.xml')
    input_files = [i['name'] for i in files]
    for file in needed:
        if file not in input_files: status = False
    if not status:
        raise ErrNeededFile()

def check_styles(doc_styles):
    from .schemas import STYLES
    status = True
    incorrect_styles = []
    for style in doc_styles:
        if style not in STYLES:
            status = False
            incorrect_styles += [style]
    if not status:
        return {'name': 'check_styles', 'status': status, 'errors': incorrect_styles}

def check_etree_element(elem):
    """
    Проверка: является ли элемет элементом lxml
    """
    if isinstance(elem, etree._Element):
        return True
    raise ErrDocumentIAdd()


def check_sectpr(elem):
    tag = [
        '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}sectPr',
    ]
    if elem.tag not in tag: return True
    return False