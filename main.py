#!/usr/bin/env python3
import os
import re
from io import BytesIO
from zipfile import ZipFile, ZIP_DEFLATED
from lxml import etree
from .error import ErrPath
from .schemas import NMSPACE, FN_ID, DEFAULT_BLANK_DOCX
from .utils import MergeMixin
from .validation import check_needed_files_in_archive, check_styles
from .elements import BaseDocument, BaseFootnotes


class Docx(MergeMixin):
    """
    Базовый класс работы с .docx файлом

    _document, _footnotes, _styles - генераторы
    document, footnotes, styles - хранение, удаление идобавление
    элементов в список элементов (кроме styles, их не стоит трогать).
    """

    __slots__ = ('file', '__file_list', '__input_file', 'document', 'footnotes', 'new_name', 'valid', 'valid_messages')

    def __init__(self, file=None):
        self.file = file or DEFAULT_BLANK_DOCX
        self.__input_file = self.__get_input_file()
        self.__file_list = self.__get_file_list()
        self.document = BaseDocument(self.__get_file('document.xml'))
        self.footnotes = BaseFootnotes(self.__get_file('footnotes.xml'))
        self.valid, self.valid_messages = self.__validation()

    def __str__(self):
        return self.file

    def __repr__(self):
        return 'file: {}, path {}'\
            .format(os.path.basename(self.file), os.path.abspath(self.file))

    def __get_path(self):
        return self.file

    def __get_input_file(self):
        """
        Попытка чтения файла или ошибка
        """
        if isinstance(self.file, str):
            arch = ZipFile(os.path.abspath(self.file))
            return arch
        raise ErrPath()

    def __get_file_list(self):
        """
        Извлечение списка файлов архива
        """
        files = []
        for file in self.__input_file.filelist:
            try:
                files.append({'name': file.filename.split('/')[-1],
                              'path': file.filename,
                              'elem': etree.fromstring(self.__input_file.read(file.filename))})
            except etree.XMLSyntaxError:
                pass
        return files

    def __validation(self):
        status = True
        # валидация с вызовом ошибки
        check_needed_files_in_archive(self.__file_list) # проверка наличия нужных файлов
        # валидация без вызова ошибок
        status_report = [] # для валидаторов не вызывающих исключения
        status_report += [check_styles(self._styles_map)]
        # проверка некритических ошибок валидации
        for st in status_report:
            if st and not st.get('status'):
                status = False
                return status, status_report
        return status, None

    def __get_file(self, file):
        """
        Функция принимает навание файла (str) и отдает файл из
        прочитанного архива.
        Если ключа нет => возвращает пустой список
        """
        for fl in self.__file_list:
            if file == fl['name']:
                return fl['elem']
        return []

    @property
    def _styles(self):
        elem = self.__get_file('styles.xml')
        if isinstance(elem, etree._Element):
            yield elem.xpath('//w:style', namespaces=NMSPACE)

    @property
    def _styles_map(self):
        for style in list(self._styles)[0]:
            st = {'styleId': style.xpath('@w:styleId', namespaces=NMSPACE)[0],
                  'name': style.xpath('w:name/@w:val', namespaces=NMSPACE)[0]}
            yield st

    @property
    def _document(self):
        elem = self.__get_file('document.xml')
        if isinstance(elem, etree._Element):
            yield elem.xpath('//w:p', namespaces=NMSPACE)

    @property
    def _footnotes(self):
        elem = self.__get_file('footnotes.xml')
        if isinstance(elem, etree._Element):
            yield elem.xpath('//w:footnotes', namespaces=NMSPACE)

    def save(self, new_name=None):
        """
        Функция сохранения файла

        new_name        : _path_/filename.docx
        """

        # подготовка
        self.document._rebuild_xml() # пересборка document.xml
        self.footnotes._rebuild_xml() # пересборка footnotes.xml
        name = new_name or self.file

        # формирование выходного файла в памяти
        memory_file = BytesIO()
        with ZipFile(memory_file, 'w', compression=ZIP_DEFLATED) as zf:
            for file in self.__file_list:
                zf.writestr(file['path'], etree.tostring(file['elem']))

        # сохранение файла в файловую систему
        with open(name, 'wb') as f:
            f.write(memory_file.getvalue())
            f.close()
