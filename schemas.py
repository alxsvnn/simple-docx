#!/usr/bin/env python3
import os

NMSPACE = {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}

FN_ID = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}id'

STYLES = [
    {'styleId': 'a', 'name': 'Normal'},
    {'styleId': 'a0', 'name': 'Default Paragraph Font'},
    {'styleId': 'a1', 'name': 'Normal Table'},
    {'styleId': 'a2', 'name': 'No List'},
    {'styleId': 'a3', 'name': 'footnote text'},
    {'styleId': 'a4', 'name': 'Текст сноски Знак'},
    {'styleId': 'a5', 'name': 'footnote reference'},
    {'styleId': 'a6', 'name': 'Table Grid'},
    {'styleId': 'Stanza', 'name': 'Stanza'},
    {'styleId': 'FootNote', 'name': 'FootNote'},
    {'styleId': '1', 'name': 'Сетка таблицы1'},
    {'styleId': 'a7', 'name': 'Normal (Web)'},
    {'styleId': '2', 'name': 'Сетка таблицы2'},
    {'styleId': 'a8', 'name': 'Hyperlink'},
    {'styleId': 'a9', 'name': 'header'},
    {'styleId': 'aa', 'name': 'Верхний колонтитул Знак'},
    {'styleId': 'ab', 'name': 'footer'},
    {'styleId': 'ac', 'name': 'Нижний колонтитул Знак'}
]

DEFAULT_BLANK_DOCX = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  'tpl/vol_src.docx')