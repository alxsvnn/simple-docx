#!/usr/bin/env python3
from lxml import etree


def page_break():
    xml = '''
    <w:p xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
    <w:pPr></w:pPr><w:r><w:br w:type="page"/></w:r></w:p>'''
    return etree.fromstring(xml)

def add_title(text=''):
    xml = '''
    <w:p xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
    <w:pPr><w:spacing w:afterLines="0"/><w:ind w:firstLine="0"/>
    <w:jc w:val="center"/><w:rPr><w:b/></w:rPr></w:pPr>
    <w:r><w:rPr><w:b/></w:rPr>
    <w:t>{}</w:t>
    </w:r></w:p>
    '''.format(text)
    return etree.fromstring(xml)

def add_p(text='', first_line=0):
    '''
    Вставка параграфа с текстом (чистый - чистая строка)
        first_line: 0 - без отступа, 567 - 1см отступ
    '''
    xml = '''
    <w:p xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
    <w:pPr><w:spacing w:afterLines="0"/><w:ind w:firstLine="{}"/>
    <w:jc w:val="center"/><w:rPr></w:rPr></w:pPr>
    <w:r><w:rPr></w:rPr><w:t>{}</w:t></w:r>
    </w:p>
    '''.format(first_line, text)
    return etree.fromstring(xml)

def add_date(text=''):
    xml = '''
    <w:p xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
    <w:pPr><w:spacing w:afterLines="0"/><w:ind w:firstLine="0"/>
    <w:jc w:val="center"/><w:rPr><w:i/></w:rPr></w:pPr>
    <w:r><w:rPr><w:i/></w:rPr>
    <w:t>{}</w:t>
    </w:r></w:p>
    '''.format(text)
    return etree.fromstring(xml)

def gen_tbl():
    xml = '''
    <w:tbl xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
    <w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblInd w:w="10" w:type="dxa"/>
    <w:tblCellMar><w:left w:w="10" w:type="dxa"/><w:right w:w="10" w:type="dxa"/>
    </w:tblCellMar>
    <w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/>
    </w:tblPr><w:tblGrid><w:gridCol w:w="1050"/><w:gridCol w:w="8315"/>
    </w:tblGrid>
    </w:tbl>
    '''
    return etree.fromstring(xml)

def gen_tr(cell_one='', cell_two=''):
    xml =  '''
    <w:tr xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
    <w:tc><w:tcPr><w:tcW w:w="1050" w:type="dxa"/><w:tcMar>
    <w:right w:w="28" w:type="dxa"/></w:tcMar></w:tcPr>
    <w:p><w:pPr><w:spacing w:afterLines="0"/><w:ind w:firstLine="0"/></w:pPr>
    <w:r><w:t>{}</w:t></w:r>
    </w:p></w:tc>
    <w:tc xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
    <w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr>
    <w:p><w:pPr><w:spacing w:afterLines="0"/><w:ind w:firstLine="0"/></w:pPr>
    <w:r><w:t>{}</w:t></w:r>
    </w:p></w:tc>
    </w:tr>
    '''.format(cell_one, cell_two)
    return etree.fromstring(xml)

def sectPr():
    xml = '''
    <w:sectPr xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mo="http://schemas.microsoft.com/office/mac/office/2008/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:mv="urn:schemas-microsoft-com:mac:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" w:rsidR="00871D1E" w:rsidRPr="00F43C80"><w:headerReference w:type="even" r:id="rId6"/><w:headerReference w:type="default" r:id="rId7"/><w:footerReference w:type="even" r:id="rId8"/><w:footerReference w:type="default" r:id="rId9"/><w:headerReference w:type="first" r:id="rId10"/><w:footerReference w:type="first" r:id="rId11"/><w:pgSz w:w="11906" w:h="16838"/><w:pgMar w:top="1134" w:right="850" w:bottom="1134" w:left="1701" w:header="708" w:footer="708" w:gutter="0"/><w:cols w:space="708"/><w:docGrid w:linePitch="360"/></w:sectPr>'''
    return etree.fromstring(xml)